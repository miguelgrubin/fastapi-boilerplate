from src.shared.domain.hex.single_value_object import SingleValueObject


class EmailAddress(SingleValueObject[str]):
    pass
