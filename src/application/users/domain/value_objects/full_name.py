from src.shared.domain.hex.value_object import VauleObject


class FullName(VauleObject):
    first_name: str
    last_name: str
