from src.shared.domain.hex.single_value_object import SingleValueObject


class PasswordHash(SingleValueObject[str]):
    pass
