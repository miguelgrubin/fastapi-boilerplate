from datetime import datetime

from src.shared.domain.hex.single_value_object import SingleValueObject


class DatetimeWrapper(SingleValueObject[datetime]):
    pass
